#-*- coding: utf-8 -*-

import os

class Mecab(object):
	"""Mecabさん用のMod"""
	def parseToNode(self, text):
		enc = u"cp932"
		tmp1 = u"tmp"
		tmp2 = u"tmp2"
		try:
			#Mecabに投げる
			open(tmp1, "w").write(text.encode(enc))
			s = "mecab %s > %s" % (tmp1, tmp2)
			os.system(s)

			#結果を読み取る
			result = []
			for i in open(tmp2):
				t = (i.decode(enc).rstrip()).split(u"\t")
				if len(t) > 1:
					rs = {}
					rs["surface"] = t[0]
					u = t[1].split(u",")
					if u[6] == u"*":
						rs["base"] = t[1]
					else:
						rs["base"] = u[6]
					rs["feature"] = u[0]

					result.append(rs)

		finally:
			os.unlink(tmp1)
			os.unlink(tmp2)

		return result

if __name__ == '__main__':
	m = Mecab()
	rs = m.parseToNode(u"太郎君と花子さんは釣りに出かけました。")
	for i in rs:
		for j in i.keys():
			print j, i[j],
		print
